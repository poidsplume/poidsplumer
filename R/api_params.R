# 
# Files that get and set API parameters
# 

#' @title Settings for the Poids-Plume API 
#'
#' @description Functions to set or retrieve Poids-Plume API settings
#'
#' @param file A file containing the user to set on the first line, and the API 
#'   key on the second line 
#'
#' @param user The API user to set
#' 
#' @param key The API key to set
#' 
#' @param print_pars If TRUE, the API parmaeters are printed on the R console 
#' 
#' @param ... Arguments passed to \code{pplume_api_set} 
#' 
#' @details These functions can be used to set, get or clear the API parameters 
#'   to be used when querying data from the Poids-Plume project. The combination
#'   of user and key to use can be done using \code{pplume_api_set} and setting 
#'   the arguments \code{user} and \code{key} to their values.
#'   \code{pplume_api_set_from_file} will read a file on the disk and set the 
#'   API parameters, assuming the user to be used is on the first line, and the 
#'   key is on the second. This is useful to make sure secrets are not present in 
#'   files that are shared or checked in a git tree. 
#'   
#'   \code{pplume_api_clear} can be used to unset the API settings, and 
#'   \code{pplume_api_get} can be used to retrieve the parameters. 
#'   
#' @examples 
#' 
#' pplume_api_set(user = "monica", 
#'                key = "5fbd2ed279920a57656a8f586304bdab")
#' 
#' pplume_api_clear()
#' 

#'@rdname pplume_api
#'@export
pplume_api_clear <- function() { 
  options(PPAPI_USER = NULL, PPAPI_KEY = NULL)
}

#'@rdname pplume_api
#'@export
pplume_api_set_from_file <- function(file, ...) { 
  a <- readLines(file)
  # Remove extraneous whitespace
  a <- gsub("^ +", "", gsub(" +$", "", a))
  pplume_api_set(user = a[1], key = a[2], ...)
}

#'@rdname pplume_api
#'@export
pplume_api_set <- function(user, key, print_pars = FALSE) { 
  
  if ( print_pars ) { 
    message(paste0("Setting Poids-Plume API parameters to ", user, "/", key)) 
  } else { 
    message(paste0("Setting Poids-Plume API parameters")) 
  }
  
  options(PPAPI_USER = user, 
          PPAPI_KEY  = key)
}

#'@rdname pplume_api
#'@export
pplume_api_get <- function() { 
  
  user <- getOption("PPAPI_USER")
  if ( is.null(user) ) { 
    stop(paste0('The user for the Poids-Plume API is undefined\n', 
                'Please use pplume_api_set() to set it'))
  }
  
  key <- getOption("PPAPI_KEY")
  if ( is.null(user) ) { 
    stop(paste0('The key for the Poids-Plume API is undefined\n', 
                'Please use pplume_api_set() to set it'))
  }
  
  return( list(user = user, key = key) )
}

