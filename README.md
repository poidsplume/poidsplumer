
# Poidsplumer: R package to download data from the Poids-Plume project 

This is a small R package to download data from the Poids Plume project. It will 
require an API key that you can get from your user profile on the website. 

# Installation 

To install the latest version of this package, you can use the `devtools`
package: 

```
install.packages('devtools')
devtools::install_git("https://gitlab.com/poidsplume/poidsplumer")
```

# Usage 

The main function in this package is `get_data`, which takes three arguments: the
feeder and the date (YYYY-MM-DD) for which you want data, and the type of data 
you want. For example, the following command line will download raw data for 
the feeder "pyrrhula" on Decembre 21st, 2020: 

```
library(poidsplumer)
pplume_api_set_from_file(<file_with_credentials>) # or use pplume_api_set
dat <- get_data("pyrrhula", "2020-12-21", "rawdata")
```

`get_data` always returns a data.frame. The columns and content of the data 
are described in more details on the [wiki](https://wiki.poids-plume.fr/index.php/Donn%C3%A9es_t%C3%A9l%C3%A9charg%C3%A9esl). 

At the moment, the data type (third argument or `get_data()`) can be one of: 

* `rawdata`, to obtain the raw values read by the feeder sensors 

* `filtdata`, to obtain the filtered values read by the feeder sensors (smoothed 
and with outliers removed)

* `visits`, to obtain data about the bird visits to the feeder 

More information can be accessed to the documentation of the function by typing
`?get_data` at the R prompt. 

# Usage notes

DOWNLOADING DATA THROUGH THIS PACKAGE REQUIRES AN ACCOUNT ON THE POIDS-PLUME 
DATA WEBSITE, AND IMPLIES ACCEPTANCE OF THE API TERMS AND CONDITIONS 

ABUSE OF THE POIDS-PLUME API MAY RESULT IN SUSPENSION OF THE API ACCESS OR THE ONLINE ACCOUNT OR PERMANENT BAN FROM THE 'ASSOCIATION'. HAVE COMMON SENSE AND 
BE RESPECTFUL IN YOUR API USAGE. 

